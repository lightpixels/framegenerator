#include "framegenerator.h"

#include <cstring>
#include <iostream>
#include <ctime>
#include <random>

FrameGenerator::FrameGenerator(size_t pixels,
                               short unsigned int nFrames,
                               short unsigned int nUpdatePercentage,
                               short unsigned int nUpdateChunkSize) :
    m_nPixels(pixels),
    m_nFrames(nFrames),
    m_nUpdatePercentage(nUpdatePercentage),
    m_nUpdateChunkSize(nUpdateChunkSize),
    m_nFrameNumber(0)
{
    m_nPossibleIndexes = m_nPixels / m_nUpdateChunkSize;
    m_nIndexesToPick = m_nPixels / m_nUpdateChunkSize * m_nUpdatePercentage / 100;

    //! Allocate a table of pointers:
    m_Frames = new uint16_t*[m_nFrames];

    //! Allocate each frame:
    for(int i = 0; i < m_nFrames; ++i) {
        m_Frames[i] = new uint16_t[m_nPixels];
    }

    //! Initialize a set of possible indexes for the chunks:
    for(size_t index = 0; index < m_nPossibleIndexes; ++index) {
        m_setIndexesAvailable.insert(index);
    }

    //! Initialize a simple vector to update the chunks:
    for(size_t index = 0; index < m_nUpdateChunkSize; ++index) {
        m_vecChunkIndexes.push_back(index);
    }
}
FrameGenerator::~FrameGenerator()
{
    for(int i = 0; i < m_nFrames; ++i) {
        delete[] m_Frames[i];
    }
    delete[] m_Frames;
}

void FrameGenerator::generateFrames()
{
    m_nFrameNumber = 0;

    //! Generate the first frame randomly:
    std::uniform_int_distribution<uint16_t> distribution;
    auto engine = std::default_random_engine(std::random_device{}());
    auto generator = std::bind(distribution, engine);

    std::generate_n(m_Frames[0], m_nPixels, generator);

    //! Generate the next frames randomly:
    srand(time(NULL));
    for(int i = 0; i < m_nFrames-1; ++i) {
        generateNextFrame(m_Frames[i],m_Frames[i+1]);
    }
}
void FrameGenerator::generateNextFrame(uint16_t const * const in, uint16_t * const out) {
    std::memcpy(out,in,sizeof(uint16_t)*m_nPixels);

    std::set<size_t> l_setIndexesAvailable = m_setIndexesAvailable;
    std::set<size_t> l_setIndexesPicked;

    //! Generate the set of Indexes picked:
    while(l_setIndexesPicked.size() != m_nIndexesToPick) {
        size_t l_ulKeyIndexToPick = rand()%l_setIndexesAvailable.size();

        size_t ulIndex = 0;
        auto l_itIndexPicked = l_setIndexesAvailable.begin();

        while(ulIndex < l_ulKeyIndexToPick) { ++l_itIndexPicked; ++ulIndex; }

        l_setIndexesPicked.insert(*l_itIndexPicked);
        l_setIndexesAvailable.erase(l_itIndexPicked);
    }

    //! Update uint16_t * const out depending on the Indexes picked:
    auto itIndexPicked = l_setIndexesPicked.begin();
    while(itIndexPicked != l_setIndexesPicked.end()) {
        size_t l_ulUpdateChunkStartIndex = (*itIndexPicked)*m_nUpdateChunkSize;

        for (auto l_ulChunkIndex: m_vecChunkIndexes) {
            out[l_ulUpdateChunkStartIndex + l_ulChunkIndex] += 1;
        }
        ++itIndexPicked;
    }
}

size_t FrameGenerator::frameSize()
{
    return m_nPixels;
}

unsigned short FrameGenerator::nFrames()
{
    return m_nFrames;
}
uint16_t* FrameGenerator::read()
{
    if(m_nFrameNumber < m_nFrames) {
        return m_Frames[m_nFrameNumber++];
    } else {
        m_nFrameNumber = 0;
        return nullptr;
    }
}
