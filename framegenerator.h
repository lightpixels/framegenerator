#ifndef FRAMEGENERATOR_H
#define FRAMEGENERATOR_H

#include "framegenerator_global.h"

#include <algorithm>
#include <set>
#include <vector>
#include <functional>
#include <stdint.h>


class FRAMEGENERATORSHARED_EXPORT FrameGenerator
{
public:
    FrameGenerator(size_t pixels = 320*240,
                   short unsigned int nFrames = 100,
                   short unsigned int nUpdatePercentage = 20,
                   short unsigned int nUpdateChunkSize = 10);
    ~FrameGenerator();

    void generateFrames();
    size_t frameSize();
    short unsigned int nFrames();
    uint16_t* read();

private:
    void generateNextFrame(uint16_t const * const in, uint16_t * const out);

    std::set<size_t> m_setIndexesAvailable;
    std::vector<size_t> m_vecChunkIndexes;
    size_t m_nPossibleIndexes;
    size_t m_nIndexesToPick;

    uint16_t** m_Frames;

    size_t m_nPixels;
    short unsigned int m_nFrames;
    short unsigned int m_nUpdatePercentage;
    short unsigned int m_nUpdateChunkSize;

    unsigned short int m_nFrameNumber;
};

#endif // FRAMEGENERATOR_H
