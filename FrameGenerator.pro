#-------------------------------------------------
#
# Project created by QtCreator 2016-09-17T16:35:41
#
#-------------------------------------------------

QT       -= gui

TARGET   = FrameGenerator

CONFIG += C++11

TEMPLATE = lib

include( ../deploy.pri )
DESTDIR = $$PROJECT_ROOT_DIRECTORY/deploy

DEFINES += FRAMEGENERATOR_LIBRARY

SOURCES += framegenerator.cpp

HEADERS += framegenerator.h\
           framegenerator_global.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
